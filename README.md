# PASSY Chrome extension
[![Join our Discord](https://discordapp.com/api/guilds/324602899839844352/widget.png?style=shield)](https://discord.gg/5K6XDnR)

This is the official extension for the PASSY password manager.

## Usage
Currently you can only install this extension manually.
You need to open the extension page in your Chromium based browser and enable the developer mode.
Then you have to drag and drop the extension into the extensions window.

## Note
The extension in it's current state will be disabled after a restart of the browser.

## License
This project is licensed under Apache License 2.0
