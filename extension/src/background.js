const port = chrome.runtime.connect({name: "background"});
let activeField = null;

function handleForSave() {

    if (activeField == null)
        return;

    let type = activeField.getAttribute("type"),
        name = activeField.getAttribute("name"),
        id = activeField.getAttribute("id");

    type = type ? type.toLowerCase() : ""; // lowercase name if not null, otherwise empty
    name = name ? name.toLowerCase() : ""; // lowercase name if not null, otherwise empty
    id = id ? id.toLowerCase() : ""; // lowercase id if not null, otherwise empty

    if (type === "text" || type === "email") {
        if (name.indexOf("name") !== -1 || id.indexOf("name") !== -1 ||
            name.indexOf("mail") !== -1 || id.indexOf("mail") !== -1 ||
            name.indexOf("user") !== -1 || id.indexOf("user") !== -1) {
            if (port)
                port.postMessage({action: "callback-save", field: "username", value: activeField.value});
        }
    } else if (type === "password") {
        if (port)
            port.postMessage({action: "callback-save", field: "password", value: activeField.value});
    }
}

if (port)
    port.onMessage.addListener(function (msg) {
        switch (msg.action) {
            case "insert":
                if (activeField) {
                    activeField.value = msg.password;
                    activeField.style.backgroundColor = "";
                }
                break;

            case "generate-pass":
                if (activeField)
                    activeField[0].form.querySelectorAll("input").forEach(function (elem) {
                        if (elem.getAttribute("type") === "password")
                            elem.val(msg.password);
                    });
                break;

            case "update-save":
                if (activeField) {
                    port.postMessage({action: "callback-save", field: msg.field, value: activeField.val()});
                }
                break;
        }
    });

function _inputFocus(e) {
    let elem = e.target,
        elemType = elem.getAttribute("type");

    if (elemType == null || elemType.toLowerCase() !== "password")
        return;

    _inputDblClick(e, true, false)
}

function _inputDblClick(e, ignoreShift = false, toggle = true) {
    if (!ignoreShift && !e.shiftKey)
        return;

    let elem = e.target,
        nodeName = elem.nodeName.toLowerCase();

    if (nodeName !== "input" && nodeName !== "textarea")
        return;

    if (activeField) {
        activeField.style.backgroundColor = "";
        activeField.style.color = "";
        if (toggle && activeField === elem) { // toggle if same input is triggered again
            activeField = null;
            return;
        }
    }

    activeField = elem;
    activeField.style.backgroundColor = "yellow";
    activeField.style.color = "black";
    handleForSave();
}

document.addEventListener("focusin", _inputFocus);
document.addEventListener("dblclick", _inputDblClick); // force select

